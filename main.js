// creo una constante la cual igualo a lo que importe la libreria express
const express = require('express');
const path = require('path');
const app = express()
const mongoose = require('mongoose');
const cors = require('cors');
const User = require('./models/userModel');
const Tarea = require('./models/tareasModel');

// hacer que la app utilice json
app.use(express.json())

// hacer que la app urilice cors
app.use(cors())

// Conectar la app con mongo usando mongoose
mongoose.connect(
    'mongodb+srv://admin:admin2021@cluster0.yhtsl.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    {useNewUrlParser: true, useUnifiedTopology: true},
    (error) => {
        if(error) {
            console.log('Hubo un error al conectarse a la base de datos: ', error);
        } else {
            console.log('Conectado a la base de datos');
        }
    }
)
//routes(rutas)

// app.get('/cargarHTML', (req, res) => {
//     res.sendFile(path.join(__dirname+'/vistas/holaMundo.html'))
// })

app.get('/', (req, res) => {
    res.send('Mi backend esta vivo y listo para trabajar')
})

app.get('/user/JorgeMartinez', (req, res) => {
    res.json({
        name: "Jorge Martinez",
        edad: 25,
        hobbies: [
            'Escuchar musica',
            'Jugar al Play'
        ]
    })
})

//RUTAS DEL CRUD DE TAREAS

// RUTA PARA DAR DE ALTA UNA TAREA (CREATE)

app.post('/tarea/user/:id', (req, res) => {
    const userId = req.params.id;
    const tareaRequestBody = req.body;
    //Validar y obtener el usuario que el cliente nos manda
    User.findById(userId, (error, user) => {
        if(error) {
            res.status(400).json({
                ok: false,
                msg: 'Hubo un problema al cargar el usuario'
            })
        } else {
            if(user) {
                //Guardamos la tarea en la db
                Tarea.create(tareaRequestBody, (error, tarea) => {
                    if(error) {
                        res.status(500).json({
                            ok: false,
                            msg: 'Hubo un problema al guardar una tarea'
                        })
                    } else {
                        //Empujamos la tarea al arreglo de traeas del usuario ya validado
                        user.tareas.push(tarea)
                        //Guardamos la modificacion que hicimos al usuario
                        user.save()
                        res.json({
                            ok: true,
                            message: 'La tarea ha sido creada'
                        })
                    }
                })
            }
        }
    })
})

// RUTAS DEL CRUD DEL USER

// RUTA PARA DAR DE ALTA UN USUARIO (CREATE)
app.post('/user', (req, res) => {
    const user = req.body;
    User.create(user, (error, user) => {
        if(error) {
            res.status(400).json({
                ok: false,
                msg: 'Hubo un problema al guardar el usuario'
            })
        } else {
            res.json({
                ok: true,
                message: 'El usuario ha sido creado',
                id: user._id
            })
        }
    })
})

//RUTA PARA TRAER TODOS LOS USUARIOS (READ)
app.get('/users/all', (req, res) => {
    User.find((error, users) => {
        if(error) {
            res.status(500).json({
                ok: false,
                msg: 'Hubo un problema al traer los usuario',
                // error: error
                error
            })
        } else {
            res.json({
                ok: true,
                // users: users
                users
            })
        }
    })
})

//RUTA PARA TRAER UN SOLO USUARIO POR ID (READ)
app.get('/user/:id', (req, res) => {
    const idQueryParam = req.params.id
    User.findById(idQueryParam).populate('tareas').exec((error, user) => {
        if(error) {
            res.status(500).json({
                ok: false,
                msg: `Hubo un problema al traer el usuario con id ${idQueryParam}`,
                // error: error
                error
            })
        } else {
            res.json({
                ok: true,
                //user: user
                user
            })
        }
    })
})

//RUTA PARA TRAER UN SOLO USUARIO POR NOMBRE (READ)
app.get('/user/byName/:nombre', (req, res) => {
    const nombreQueryParam = req.params.nombre
    User.find({nombre: nombreQueryParam}, (error, user) => {
        if(error) {
            res.status(400).json({
                ok: false,
                msg: `Hubo un problema al traer el usuario con nombre ${nombreQueryParam}`,
                // error: error
                error
            })
        } else {
            res.json({
                ok: true,
                user
            })
        }
    })
})

//RUTA PARA ACTUALIZAR UN UAUSARIO (UPDATE)
app.put('/user/actualizar/:id', (req, res) => {
    const idQueryParam = req.params.id
    const userUpdated = req.body
    User.findByIdAndUpdate(idQueryParam, userUpdated, {new: true}, (error, user) => {
        if(error) {
            res.status(400).json({
                ok: false,
                msg: `Hubo un problema al acyualizar el usuario con id ${idQueryParam}`,
                // error: error
                error
            })
        } else {
            res.json({
                ok: true,
                user
            })
        }
    })
})

//RUTA PARA ELIMINAR UN UAUSARIO (DELETE)
app.delete('/user/delete/:id', (req, res) => {
    const userId = req.params.id
    User.findByIdAndDelete(userId, (error) => {
        if(error) {
            res.status(400).json({
                ok: false,
                msg: `Hubo un problema al borrar el usuario con id ${userId}`,
                // error: error
                error
            })
        } else {
            res.json({
                ok: true,
                msg: 'El usuario ha sido borrado con exito'
            })
        }
    })
})

// le digo a mi app en que puerto corra
// Puertos disponible 3000, 3001, 5000, 5001, 8080, 8081
app.listen(8080, (error) => {
    if(error) {
        console.log('Hubo un error al iniciar la app', error);
    } else {
        console.log('La app esta viva en el puerto 3001');
    }
})