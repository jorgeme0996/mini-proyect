const mongoose = require('mongoose');

const TareaSchema = new mongoose.Schema({
    nombre: String,
    descripcion: String,
    createdAt: {type: Date, default: new Date}
})

module.exports = mongoose.model("Tarea", TareaSchema)