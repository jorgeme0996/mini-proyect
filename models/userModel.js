const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    nombre: String,
    apellido: String,
    edad: Number,
    tareas: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Tarea"
        }
    ]
})

module.exports = mongoose.model("User", UserSchema)