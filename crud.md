# CRUD

- Create (crear)
- Read (leer)
- Update (actualizar)
- Delete (borrar)

## Verbos de una Rest API

- POST -> Create (crear)
- GET -> Read (leer)
- PUT o PATCH -> Update (actualizar)
- DELETE -> Delete (borrar)